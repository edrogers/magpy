resource "aws_iam_role" "deployment_role" {
  name                  = "MagpyDeployment"
  force_detach_policies = true
  assume_role_policy    = data.aws_iam_policy_document.trusted_principals.json
}

data "aws_iam_policy_document" "trusted_principals" {
  statement {
    sid     = "TrustedPrincipals"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
  }
}

resource "aws_iam_policy" "access_remote_state" {
  name   = "AccessTerraformRemoteStateBucket"
  policy = data.aws_iam_policy_document.access_remote_state.json
}

data "aws_iam_policy_document" "access_remote_state" {
  statement {
    sid       = "ListRemoteStateBucket"
    actions   = ["s3:ListObject"]
    resources = [aws_s3_bucket.remote_state.arn]
  }

  statement {
    sid = "ReadWriteRemoteStateBucketObjects"
    actions = [
      "s3:GetObject",
      "s3:PutObject",
    ]
    resources = ["${aws_s3_bucket.remote_state.arn}/*"]
  }
}

locals {
  deployment_role_policy_arns = [
    aws_iam_policy.access_remote_state.arn,
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess",
  ]
}

resource "aws_iam_role_policy_attachment" "deployment_role_policies" {
  count      = length(local.deployment_role_policy_arns)
  role       = aws_iam_role.deployment_role.name
  policy_arn = local.deployment_role_policy_arns[count.index]
}
