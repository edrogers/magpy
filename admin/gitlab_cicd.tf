locals {
  gitlab_project_id = "19058202" # madpy/magpy
}

resource "gitlab_project_variable" "aws_access_key_id" {
  project           = local.gitlab_project_id
  key               = "AWS_ACCESS_KEY_ID"
  value             = aws_iam_access_key.automation_user.id
  environment_scope = "*"
}

resource "gitlab_project_variable" "aws_secret_access_key" {
  project           = local.gitlab_project_id
  key               = "AWS_SECRET_ACCESS_KEY"
  value             = aws_iam_access_key.automation_user.secret
  environment_scope = "*"
  masked            = true
}