terraform {
  required_version = "~>0.12.26"
  backend "s3" {
    bucket       = "madpy-terraform-remote-state"
    key          = "magpy/infra.tfstate"
    region       = "us-west-2"
    role_arn     = "arn:aws:iam::360882455069:role/MagpyDeployment"
    session_name = "gitlab-ci"
  }
}

provider "aws" {
  region  = "us-west-2"
  version = "~>2.63.0"
  assume_role {
    role_arn     = "arn:aws:iam::360882455069:role/MagpyDeployment"
    session_name = "gitlab-ci"
  }
}

resource "aws_ecr_repository" "magpy" {
  name = "magpy"
}
